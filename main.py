import pickle
import xml.etree.ElementTree as ET
from pronto import Ontology
import random

ONTOLOGY_PATH = './data/hp.obo'
ONTOLOGY_ROOT = "HP:0000118"
HPO_FILENAME = "./data/en_product4_HPO.xml"
GRAPH_PATH = "data/pruned.dictionary"

global ont, hpo, graph, answers, pending, visited_keys, num_question

def load_data():
    global ont, hpo, graph
    print("Loading pickle...")
    with open('data/pruned.dictionary', 'rb') as file:
        graph = pickle.load(file)
    print("... loading Ontology ...")
    #ont = Ontology(ONTOLOGY_PATH)
    print("... loading HPO ...")
    #hpo = ET.parse(HPO_FILENAME).getroot()
    print("... done loaded.")

def get_answer(question):
    return random.choice([True, False, False])

def get_key_sort(x):
    if x is None or not "weight" in x or type(x["weight"]) is not int:
        a = 3
    return x["weight"]

def get_next_node():
    global pending, visited_keys
    if len(pending) == 0:
        return None
    pending.sort(key=get_key_sort)
    node = pending.pop()
    if node["id"] in visited_keys:
        return get_next_node()
    else:
        visited_keys.append(node["id"])
        return node


def init_structures():
    global answers, pending, visited_keys, num_question
    num_question = 0
    answers = []
    visited_keys = []
    pending = graph["childs"]



def print_question(question):
    global num_question
    print(f"#{num_question} - Question node: {question['id']}", end=" ")


def process_answer(question, answ):
    global answers, pending
    print(f"-> {answ}")
    answers.append((answ, question))
    if answ:
         pending = pending + question["childs"]


def start_questionary():
    global num_question
    question = get_next_node()
    while question is not None:
        num_question += 1
        print_question(question)
        answ = get_answer(question)
        process_answer(question, answ)
        question = get_next_node()

if __name__ == "__main__":
    load_data()
    values = []
    for i in range(1):
        init_structures()
        start_questionary()
        values.append(num_question)

    m = sum(values)/len(values)
    print(f"MEAN: {m}")

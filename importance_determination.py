from pronto import Ontology
import pronto
import xml.etree.ElementTree as ET
import pickle

from tqdm import tqdm

ONTOLOGY_PATH = './data/hp.obo'
ONTOLOGY_ROOT = "HP:0000118"
HPO_FILENAME = "./data/en_product4_HPO.xml"

global ont, hpo, tqdm_h
acc = 0

def load_data():
    global ont, hpo
    ont = Ontology(ONTOLOGY_PATH)
    hpo = ET.parse(HPO_FILENAME).getroot()


def parse_phenotype(phenotype):
    id = phenotype.find("HPO").find("HPOId").text
    id_clean = get_main_id(id)
    freq:str = phenotype.find("HPOFrequency").find("Name").text.lower()
    importance = 0
    if "always" in freq:
        importance = 100
    elif "very" in freq:
        importance = 90
    elif "frequent" in freq:
        importance = 55
    elif "ocasional" in freq:
        importance = 20
    
    return id_clean, importance


def add_importance_to_node(node, importance=0):
    if node.comment is not None and type(node.comment) is int:
        importance += node.comment
    node.comment = importance
    return node.comment


def update_tree(phenotype, importance):
    node = ont[phenotype]
    add_importance_to_node(node, importance)
    #print(f"Node {phenotype} with value {importance}")

def update_phenotype(phenotype):
    phenotype, importance = parse_phenotype(phenotype)
    if importance != 0:
        update_tree(phenotype, importance)


def get_main_id(phenotype):
    if phenotype in ont:
        return phenotype
    else:
        last = None
        for node in ont[ONTOLOGY_ROOT].subclasses():
            if phenotype in node.alternate_ids:
                last = node.id
        if last is not None:
            return last
    raise Exception(f"Unkown phenotype: {phenotype}")


def aggregate_weights(node:pronto.Term):
    global tqdm_h
    it = node.subclasses(1)
    next(it)
    try:
        subnode = next(it)
    except StopIteration:
        tqdm_h.update(1)
        return add_importance_to_node(node, 0)

    acc = 0
    acc += aggregate_weights(subnode)
    for subnode in it:
        acc += aggregate_weights(subnode)
    tqdm_h.update(1)
    return add_importance_to_node(node, acc)

def prune(node):
    if node.comment == 0:
        return None
    graph = {
        "weight": node.comment,
        "id": node.id,
        "ids": list(node.alternate_ids)+[node.id],
        "childs": []
    }
    it = node.subclasses(1)
    next(it)
    try:
        subnode = next(it)
    except StopIteration:
        return graph

    val = prune(subnode)
    if val is not None:
        graph["childs"].append(val)
    for subnode in it:
        val = prune(subnode)
        if val is not None:
            graph["childs"].append(val)
    return graph


def save_and_prune():
    graph = prune(ont[ONTOLOGY_ROOT])
    with open('data/pruned.dictionary', 'wb') as handle:
        pickle.dump(graph, handle)



def main():
    pbar = tqdm(total=int(hpo[1].get("count")))
    for disorder in hpo[1].getiterator("Disorder"):
        for phenotype in disorder.getiterator("HPODisorderAssociation"):
            update_phenotype(phenotype)
        pbar.update(1)
    pbar.close()
    print("Aggregating weights...")
    global tqdm_h
    tqdm_h = tqdm(total=len(ont))
    aggregate_weights(ont[ONTOLOGY_ROOT])
    tqdm_h.close()
    save_and_prune()



if __name__ == "__main__":
    print("Loading data...")
    load_data()
    print("...data loaded.")
    main()
    print(f"Root value: {ont[ONTOLOGY_ROOT].comment}")

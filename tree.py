from pronto import Ontology
import pronto
import xml.etree.ElementTree as ET
import pickle

from tqdm import tqdm

ONTOLOGY_PATH = './data/hp.obo'
ONTOLOGY_ROOT = "HP:0000118"
HPO_FILENAME = "./data/en_product4_HPO.xml"

global ont, hpo, tqdm_h


def load_data():
    global ont, hpo
    ont = Ontology(ONTOLOGY_PATH)
    hpo = ET.parse(HPO_FILENAME).getroot()


def add_importance_to_node(node, importance=0):
    if node.comment is not None and type(node.comment) is int:
        importance += node.comment
    node.comment = importance
    return node.comment


def parse_phenotype(phenotype):
    id = phenotype.find("HPO").find("HPOId").text
    id_clean = get_main_id(id)
    freq: str = phenotype.find("HPOFrequency").find("Name").text.lower()
    importance = 0
    if "always" in freq:
        importance = 100
    elif "very" in freq:
        importance = 90
    elif "frequent" in freq:
        importance = 55
    elif "ocasional" in freq:
        importance = 20

    return id_clean, importance

def update_tree(phenotype, importance):
    node = ont[phenotype]
    add_importance_to_node(node, importance)
    #print(f"Node {phenotype} with value {importance}")

def update_phenotype(phenotype):
    phenotype, importance = parse_phenotype(phenotype)
    if importance != 0:
        update_tree(phenotype, importance)

def main():
    for disorder in hpo[1].getiterator("Disorder"):
        for phenotype in disorder.getiterator("HPODisorderAssociation"):
            update_phenotype(phenotype)


if __name__ == "__main__":
    print("Loading data...")
    load_data()
    print("...data loaded.")
    main()

